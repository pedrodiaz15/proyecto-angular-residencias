import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GatoVersusComponent } from './gato-versus.component';

describe('GatoVersusComponent', () => {
  let component: GatoVersusComponent;
  let fixture: ComponentFixture<GatoVersusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GatoVersusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GatoVersusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
