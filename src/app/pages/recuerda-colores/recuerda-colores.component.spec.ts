import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecuerdaColoresComponent } from './recuerda-colores.component';

describe('RecuerdaColoresComponent', () => {
  let component: RecuerdaColoresComponent;
  let fixture: ComponentFixture<RecuerdaColoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecuerdaColoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecuerdaColoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
