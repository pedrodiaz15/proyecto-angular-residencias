import { NgModule } from '@angular/core';


import { SharedModule } from '../shared/shared.module';
import { PagesComponent } from './pages.component';
import {FormsModule} from '@angular/forms';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ProgressComponent } from './progress/progress.component';
import { Graficas1Component } from './graficas1/graficas1.component';
import { PAGES_ROUTES } from './pages.routes';
import { ChartsModule } from 'ng2-charts';



// Temporal
import { IncrementadorComponent } from '../components/incrementador/incrementador.component';
import { GraficoDonaComponent } from '../components/grafico-dona/grafico-dona.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { PromesasComponent } from './promesas/promesas.component';
import { RxjsComponent } from './rxjs/rxjs.component';
import { JuegosComponent } from './juegos/juegos.component';
import { GatoComponent } from './gato/gato.component';
import { GatoVersusComponent } from './gato-versus/gato-versus.component';
import { AhorcadoComponent } from './ahorcado/ahorcado.component';
import { RecuerdaColoresComponent } from './recuerda-colores/recuerda-colores.component';
import { CulebritaComponent } from './culebrita/culebrita.component';
import { GroupsComponent } from './groups/groups.component';
import { P1Component } from './p1/p1.component';
import { P2Component } from './p2/p2.component';
import { P4Component } from './p4/p4.component';
import { P5Component } from './p5/p5.component';
import { P6Component } from './p6/p6.component';
import { P7Component } from './p7/p7.component';
import { P3Component } from './p3/p3.component';




@NgModule({
  declarations: [
    PagesComponent,
    DashboardComponent,
    ProgressComponent,
    Graficas1Component,
    IncrementadorComponent,
    GraficoDonaComponent,
    AccountSettingsComponent,
    PromesasComponent,
    RxjsComponent,
    JuegosComponent,
    GatoComponent,
    GatoVersusComponent,
    AhorcadoComponent,
    RecuerdaColoresComponent,
    CulebritaComponent,
    GroupsComponent,
    P1Component,
    P2Component,
    P4Component,
    P5Component,
    P6Component,
    P7Component,
    P3Component,
    // SettingsService
  ],
  exports: [
    DashboardComponent,
    ProgressComponent,
    Graficas1Component,
  ],
  imports: [
    SharedModule,
    PAGES_ROUTES,
    FormsModule,
    ChartsModule,
  ]
})
export class PagesModule { }
