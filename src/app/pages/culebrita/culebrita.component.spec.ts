import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CulebritaComponent } from './culebrita.component';

describe('CulebritaComponent', () => {
  let component: CulebritaComponent;
  let fixture: ComponentFixture<CulebritaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CulebritaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CulebritaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
