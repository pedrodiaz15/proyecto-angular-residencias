import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  menu: any = [
    {
      titulo: 'Seccion1',
      icono: 'mdi mid-gauge',
      submenu: [
        {titulo: 'Dashboard', url: '/dashboard'},
        {titulo: 'ProgressBar', url: '/progress'},
        {titulo: 'Graficass', url: '/graficas1'},
        {titulo: 'Login', url: '/login'},
        {titulo: 'Promesas', url: '/promesas'},
        {titulo: 'Rxjs', url: '/rxjs'},
        {titulo: 'gato', url: '/gato'},
        {titulo: 'gato-versus', url: '/gato-versus'},
        {titulo: 'ahorcado', url: '/ahorcado'},
        {titulo: 'culebrita', url: '/culebrita'},
        {titulo: 'Recuerda Colores ', url: '/recuerda-colores'},
        {titulo: 'Pantalla 1', url: '/p1'},
        {titulo: 'Pantalla 2', url: '/p2'},
        {titulo: 'Pantalla 3', url: '/p3'},
        {titulo: 'Pantalla 4', url: '/p4'},
        {titulo: 'Pantalla 5', url: '/p5'},
        {titulo: 'Pantalla 6', url: '/p6'},
        {titulo: 'Pantalla 7', url: '/p7'},

      ]
    }
  ];
  constructor() { }
}
